use "collections"

type ConfigNotify[Value=String] is {ref (Value)} ref

/**
 * A Config class maintains for each key an associated value.
 * The values in a Config are read by listening, which will
 * call a given function every time the value is updated.
 *
 * Listeners and values can be added in either order; a listener
 * will be called as soon as a value is added, or may be called
 * immediately with the first value.
 *
 * Values which are the same as the old value will not be sent again.
 * Listeners with the same identity will not be recorded twice
 * (this is not parametric)
 */
class Config[Key: (Hashable & Equatable[Key] #read) = String, Value: Equatable[Value] #read = String]
  embed data: Map[Key, Value] = Map[Key, Value]
  embed listeners: Map[Key, SetIs[ConfigNotify[Value]]] = Map[Key, SetIs[ConfigNotify[Value]]]

  new ref create() =>
    None

  fun ref update(key: Key, value: Value) =>
    let old_value = data(key) = value
    let should_report = match old_value
    | let old_value': Value => value != old_value'
    else
      true
    end
    if should_report then
      try
        for listener in listeners(key)?.values() do
          listener(value)
        end
      end
    end

  fun ref listen(key: Key, listener: ConfigNotify[Value]) =>
    listeners.insert_if_absent(key, SetIs[ConfigNotify[Value]])
      .set(listener)
    try
      let existing = data(key)?
      listener(existing)
    end

/**
 * A Configurer actor maintains a configuration, receiving
 * updates and listeners asynchronously
 *
 * This actor directly wraps a Config, and the exact same caveats apply
 */
actor Configurer[Key: (Hashable & Equatable[Key] val) = String, Value: Equatable[Value] val = String]
  embed config: Config[Key, Value] = Config[Key, Value]

  be update(key: Key, value: Value) =>
    config.update(key, value)
  be listen(key: Key, listener: ConfigNotify[Value] iso) =>
    config.listen(key, consume listener)


# Simple Asynchronous Pony Config

Here's a mini package / pattern for
asynchronous configuration.

It works like a Publish-subscribe system,
where the messages are exclusively
changes to the configuration. Both an
actor-local Config object and a
Configurer object are available

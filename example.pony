actor Example
  let _env: Env
  var _foo: String

  be _set_foo(value: String) =>
    this._foo = value
    _env.out.print(",".join([_foo].values()))

  be _fake_set(name: String, value: String) =>
    None

  new create(env: Env) =>
    let config = Configurer
    this._env = env
    this._foo = "x"

    config.listen("foo", recover this~_set_foo() end)
    config.listen("bar", recover this~_fake_set("bar") end)

    config.update("foo", "First value!")
    config.update("foo", "Another value!")

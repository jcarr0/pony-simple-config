use "ponytest"

actor Main is TestList
  new create(env: Env) =>
    PonyTest(env, this)

  new make() =>
    None

  fun tag test0(test: PonyTest, name: String, func: {(TestHelper)} val) =>
    test(recover _T(name, func) end)

  fun tag tests(test: PonyTest) =>
    test0(test, "config_listen_set", {(h) =>
      let c = Config
      let key = "foo"
      let expected = "Set1"

      let obj = SimpleReceive
      c.listen(key, obj)
      c(key) = expected

      h.assert_eq[String](obj.value, expected)
    })
    test0(test, "config_set_listen", {(h) =>
      let c = Config
      let key = "foo"
      let expected = "Set2"

      let obj = SimpleReceive
      c(key) = expected
      c.listen(key, obj)

      h.assert_eq[String](obj.value, expected)
    })
    test0(test, "config_listen_set_set", {(h) =>
      let c = Config
      let key = "foo"
      let dummy = "Bad"
      let expected = "Set3"

      let obj = SimpleReceive
      c.listen(key, obj)
      c(key) = dummy
      c(key) = expected

      h.assert_eq[String](obj.value, expected)
    })
    test0(test, "config_listen_set_other", {(h) =>
      let c = Config
      let key = "foo"
      let key2 = "bar"
      let bad = "Bad"

      let obj = SimpleReceive
      c.listen(key, obj)
      c(key2) = bad

      h.assert_eq[String](obj.value, obj.unsetValue())
    })
    test0(test, "config_set_other_listen", {(h) =>
      let c = Config
      let key = "foo"
      let key2 = "bar"
      let bad = "Bad"

      let obj = SimpleReceive
      c(key2) = bad
      c.listen(key, obj)

      h.assert_eq[String](obj.value, obj.unsetValue())
    })
    test0(test, "config_set_twice", {(h) =>
      let c = Config
      let key = "foo"
      let expected = "Set"

      let obj = object
        var count: USize = 0
        fun ref apply(new_value: String) => count = count + 1
      end
      c.listen(key, obj)
      c(key) = expected
      c(key) = expected

      h.assert_eq[USize](obj.count, 1)
    })

class SimpleReceive
  var value: String = "Never received"
  fun ref apply(value': String) =>
    value = value'
  fun unsetValue(): String => "Never received"
  new ref create() =>
    None

class iso _T is UnitTest
  let _name: String
  let _fun: {(TestHelper)} box
  new create(name': String, fun': {(TestHelper)} box) =>
    _name = name'
    _fun = fun'

  fun name(): String => _name
  fun apply(h: TestHelper) => _fun(h)
